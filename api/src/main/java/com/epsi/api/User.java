package com.epsi.api;

import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.lang.NonNull;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;

@Entity
class User {
  private @Id @GeneratedValue Long id;

  @NotBlank(message = "Email is required")
  private String email;

  @NotBlank(message = "Full name is required")
  private String fullName;

  User() {
  }

  User(String email, String fullName) {
    this.email = email;
    this.fullName = fullName;
  }

  public Long getId() {
    return this.id;
  }

  public String getEmail() {
    return this.email;
  }

  public String getFullName() {
    return this.getFullName();
  }

}
