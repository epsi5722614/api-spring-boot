package com.epsi.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping
  public String basicGetMethod() {
    return "GET";
  }

  @PostMapping
  public ResponseEntity<String> basicPostMethod(@Validated @RequestBody User user) {
    return ResponseEntity.ok("POST");
  }

  @PutMapping
  public String basicPutMethod(@RequestBody User partialUser) {
    return "PUT";
  }

  @DeleteMapping("/{id}")
  public String basicDeleteMethod(@PathVariable Long id) {
    if (userRepository.existsById(id)) {
      return "DELETE";
    }
    return "ID doesn't exist";
  }
}
